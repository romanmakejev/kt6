import java.util.*;
import java.util.Random;

/** Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {

   /** Main method. */
   public static void main (String[] args) {
      GraphTask a = new GraphTask();
      a.run();
   }

   /** Actual main method to run examples and everything. */
   public void run() {
      Graph a = new Graph ("A");

      // Result examples (used not big graphs to check data)
      int minValue = 6;
      int maxValue = 10;
      int arrowNumber = (int) (Math.random() * (maxValue - minValue + 1) + minValue);  // number of edges
      a.createRandomSimpleGraph(5, arrowNumber);
      System.out.println("1) Initial Graph:");
      System.out.println(a);
      System.out.println();

      System.out.println("Minimal spanning tree Prim:");
      System.out.println(a.minimumSpanningTree());
      System.out.println();

      System.out.println("2) Minimal spanning tree Kruskal:");
      List<Arc> k = a.kruskalMST();
      String message = "Edges of MST: [";
      for (int x = 0; x < k.size(); x++) {
         message += x == k.size() - 1 ? k.get(x).toStringWithWeight() : k.get(x).toStringWithWeight() + ", ";
      }
      message += "]";
      System.out.println(message);

      Graph b = a.cloneGraph("B");
      System.out.println();
      System.out.println("3) Graph clone:");
      System.out.println();
      System.out.println(b);
      System.out.println(a.areVerticesDifferent(a.first, b.first));
      System.out.println(a.areArcsDifferent(a.first.first, b.first.first));

      System.out.println();
      System.out.println("4) Graph longest point:");
      System.out.println();
      System.out.println(a.vertices.get(0) + "  <- input");
      System.out.println(a.longestPointFrom(a.vertices.get(0)) + "  <- farthest from " + a.vertices.get(0));

      System.out.println();
      System.out.println("5) Reflexive transitive closure of graph:");
      System.out.println();
      System.out.println(a);
      System.out.println(Arrays.deepToString(a.createAdjMatrix()));
      Graph rtc = a.createReflexiveTransitiveClosureGraphWithoutWeights();
      System.out.println(rtc);
      System.out.println(Arrays.deepToString(rtc.createAdjMatrix()));
   }

   /**
    * Vertex represents a vertex in the graph.
    */
   class Vertex {

      private String name;
      private Vertex next;
      private Arc first;
      private int info = 0;

      /**
       * Basic constructor for vertex in the graph.
       * @param name - name of the vertex.
       * @param next - next vertex in the graph.
       * @param first - first arc (arrow) related to this vertex.
       */
      Vertex (String name, Vertex next, Arc first) {
         this.name = name;
         this.next = next;
         this.first = first;
      }

      /**
       * Vertex constructor for empty vertex (only name and no related vertex or arrow)
       * @param name - name of the vertex.
       */
      Vertex (String name) {
         this (name, null, null);
      }

      /**
       * String representation of the vertex.
       * @return name of the vertex.
       */
      @Override
      public String toString() {
         return name;
      }

      /**
       * Calculates number of all related vertexes to current vertex. Calculates vertex chain depth.
       * @return number of vertexes in chain.
       */
      int getAll() {
         int vertexNumber = 1;  // Store number of vertexes in chain.
         Vertex currentVertex = this.next;
         while (currentVertex != null) {
            vertexNumber++;
            currentVertex = currentVertex.next;
         }
         return vertexNumber;
      }
   }

   /** Arc represents one arrow in the graph. Two-directional edges are
    * represented by two Arc objects (for both directions).
    */
   class Arc {

      private String name;
      private Vertex target;
      private Vertex from;
      private Arc next;
      private Integer weight;

      /**
       * Basic constructor for arrow in the graph.
       * @param name - name of the arrow.
       * @param target - target vertex of the arrow.
       * @param next - next arrow related to this one.
       */
      Arc (String name, Vertex target, Arc next) {
         this.name = name;
         this.target = target;
         this.next = next;
      }

      /**
       * Arrow constructor for empty arrow.
       * @param name - name of the arrow.
       */
      Arc (String name) {
         this (name, null, null);
      }

      /**
       * String representation of an arrow
       * @return name of the arrow.
       */
      @Override
      public String toString() {
         return name;
      }

      /**
       * String representation of an arrow with weight.
       * @return name of the arrow.
       */
      public String toStringWithWeight() {
         return name + "|" + weight + "|";
      }

      /**
       * Finds all related arrows to this one an put them into an array.
       * @return array of all related arrows.
       */
      Arc[] getRelatedArcs() {
         Arc[] relatedArcs = new Arc[0];  // Array to hold all related arrows
         Arc currentArc = this;
         while (currentArc != null) {
            // Move new relations to arrow array
            relatedArcs = Arrays.copyOf(relatedArcs, relatedArcs.length + 1);
            relatedArcs[relatedArcs.length - 1] = currentArc;
            currentArc = currentArc.next;
         }
         return relatedArcs;
      }
   }

   class Graph {

      private String name;
      private Vertex first;
      private List<Vertex> vertices = new ArrayList<>();
      private List<Arc> arcs = new ArrayList<>();
      private final List<DisjointSet> forest = new ArrayList<>();
      private int info = 0;

      /**
       * Basic constructor for Graph with arrows and vertexes.
       * @param name  - name of the Graph.
       * @param first - first Vertex in the Graph.
       */
      Graph(String name, Vertex first) {
         this.name = name;
         this.first = first;
      }

      /**
       * Graph constructor for empty Graph.
       * @param name - name of the Graph.
       */
      Graph(String name) {
         this(name, null);
      }

      /**
       * String representation of the Graph.
       * @return name of the graph with all related Vertexes with their arrows.
       */
      @Override
      public String toString() {
         String nl = System.getProperty("line.separator");
         StringBuffer sb = new StringBuffer(nl);
         sb.append(name);
         sb.append(nl);
         Vertex v = first;
         while (v != null) {
            sb.append(v.toString());
            sb.append(" -->");
            Arc a = v.first;
            while (a != null) {
               sb.append(" ");
               sb.append(a.toString());
               sb.append(" (");
               sb.append(v.toString());
               sb.append("->");
               sb.append(a.target.toString());
               sb.append(")");
               if (a.weight != null) {
                  sb.append(" |");
                  sb.append(a.weight);  // Added weight values
                  sb.append("|");
               }
               a = a.next;
            }
            sb.append(nl);
            v = v.next;
         }
         return sb.toString();
      }

      /**
       * Creates a vertex from given string.
       * @param name - name of the Vertex.
       * @return new vertex where name is given as parameter and next vertex is current one.
       */
      public Vertex createVertex(String name) {
         Vertex res = new Vertex(name);
         res.next = first;
         first = res;
         return res;
      }

      /**
       * Creates an arrow with weight.
       * @param name  - name of the arrow.
       * @param from  - arrow start destination.
       * @param to    - arrow end destination.
       * @param weigh - weight of the arrow (For example distance)
       * @return new arrow with name, start and end destination and weight.
       */
      public Arc createArc(String name, Vertex from, Vertex to, int weigh) {
         Arc res = new Arc(name);
         res.next = from.first;
         res.weight = weigh;
         res.from = from;
         from.first = res;
         res.target = to;
         arcs.add(res);
         return res;
      }

      /**
       * Creates an arrow without weight.
       * @param name  - name of the arrow.
       * @param from  - arrow start destination.
       * @param to    - arrow end destination.
       * @return new arrow with name, start and end destination and weight.
       */
      public Arc createArc(String name, Vertex from, Vertex to) {
         Arc res = new Arc(name);
         res.next = from.first;
         res.from = from;
         from.first = res;
         res.target = to;
         arcs.add(res);
         return res;
      }

      /**
       * Method to get all vertices after creating a graph.
       * Adds all vertices to an ArrayList defined in graph class.
       */
      public void getVertices() {
         List<Vertex> vertices = new ArrayList<>();
         Vertex vertex = this.first;
         while (vertex != null) {
            vertices.add(vertex);
            vertex = vertex.next;
         }
         this.vertices = vertices;
      }

      /**
       * Method to find the distance between two vertices (aka arc length) from two vertex ID strings.
       *
       * @param start - Start vertex ID
       * @param end   -  End vertex ID
       * @return arc length (integer)
       */
      public int findArcDistance(String start, String end) {
         for (Arc arc : arcs) {
            if (arc.from.name.equals(start) && arc.target.name.equals(end)) {
               return arc.weight;
            }
         }
         return 0; //return 0 if there isn't an arc between two vertices
      }

      /**
       * Create a matrix of arcs, used in getDistMatrix method to find corresponding arc lengths.
       *
       * @return Matrix of arcs.
       */
      public String[][] arcMatrix() {
         int[][] adjMatrix = createAdjMatrix();
         int nvert = adjMatrix.length;
         String[][] arcMatrix = new String[nvert][nvert];
         for (int i = 0; i < nvert; i++) {
            for (int j = 0; j < nvert; j++) {
               arcMatrix[i][j] = vertices.get(i).name + "-" + vertices.get(j).name;
            }
         }
         return arcMatrix;
      }

      /**
       * Method to find a matrix of all the distances in graph. If there is no connection between two vertices,
       * the distance is defined as infinity (in this case Integer.MAX_VALUE divided by 4, should be big enough).
       *
       * @return Matrix of all the distances.
       */
      public int[][] getDistMatrix() {
         int[][] adjMatrix = createAdjMatrix();
         int nvert = adjMatrix.length;
         int[][] distMatrix = new int[nvert][nvert];
         String[][] arcMatrix = arcMatrix();
         if (nvert < 1) return distMatrix;
         int inf = Integer.MAX_VALUE / 4;
         for (int i = 0; i < nvert; i++) {
            for (int j = 0; j < nvert; j++) {
               distMatrix[i][j] = distMatrix[i][j] * -1;
               if (adjMatrix[i][j] == 0) {
                  distMatrix[i][j] = inf;
               } else {
                  int distance = findArcDistance(arcMatrix[i][j].split("-")[0], arcMatrix[i][j].split("-")[1]);
                  distMatrix[i][j] = distance;
               }
            }
         }
         for (int i = 0; i < nvert; i++) {
            distMatrix[i][i] = 0;
         }
         return distMatrix;
      }


      /**
       * Method to find longest paths in given graph.
       *
       * @return Matrix of integers where each integer shows the longest path.
       */
      public int[][] longestPaths() {
         int[][] distMatrix = getDistMatrix();
         int n = vertices.size();// number of vertices
         if (n < 1) {
            throw new RuntimeException("Cannot find longest path in an empty graph.");
         }
         for (int k = 0; k < n; k++) {
            for (int i = 0; i < n; i++) {
               for (int j = 0; j < n; j++) {
                  int newLength = distMatrix[i][k] + distMatrix[k][j];
                  if (distMatrix[i][j] > newLength) {
                     distMatrix[i][j] = newLength; // new path is shorter
                  }
               }
            }
         }
         return distMatrix;
      }

      /**
       * Method to find the farthest point from a given vertex in given graph.
       * This is the main method that is used to solve the given task.
       *
       * @param vertex - Vertex to find the farthest point from.
       * @return - Vertex that is the farthest from given vertex.
       */
      public Vertex longestPointFrom(Vertex vertex) {
         int index = vertices.indexOf(vertex);
         int[][] distMatrix = longestPaths();
         int inf = Integer.MAX_VALUE / 4;
         int maxIntIndex = 0;
         int max = 0;
         for (int i = 0; i < distMatrix[index].length; i++) {
            if (distMatrix[index][i] != inf && distMatrix[index][i] > max) {
               max = distMatrix[index][i];
               maxIntIndex = i;
            }
         }
         return vertices.get(maxIntIndex);
      }

      /** Disjoint Set Implementation
       * This class represents a disjoint set.
       * In each set, there is a unique parent (or root) that represents this set.
       * Source: https://www.baeldung.com/java-spanning-trees-kruskal#set-implementation
       */
      class DisjointSet {
         Vertex parent;

         public Vertex getParent() {
            return parent;
         }

         public void setParent(Vertex parent) {
            this.parent = parent;
         }

         @Override
         public String toString() {
            return "parent=" + parent;
         }
      }

      /**
       * Create a connected undirected random tree with n vertices.
       * Each new vertex is connected to some random existing vertex.
       * @param n number of vertices added to this graph
       */
      public void createRandomTree(int n) {
         if (n <= 0)
            return;

         for (int i = 0; i < n; i++) {
            vertices.add(i, createVertex (String.valueOf(n - i)));
            if (i > 0) {
               int vnr = (int)(Math.random()*i);
               int weight = (int)((Math.random() * (n - 1)) + 1); // takes random weight

               // because our graph is undirected we need to add only on of the arcs to list
               arcs.add(i-1, createArc ("a" + vertices.get(vnr).toString() + "_" + vertices.get(i).toString(),
                       vertices.get(vnr), vertices.get(i), weight));
               createArc ("a" + vertices.get(i).toString() + "_" + vertices.get(vnr).toString(),
                       vertices.get(i), vertices.get(vnr), weight);
            } else {}
         }
      }

      /**
       * Create an adjacency matrix of this graph.
       * Side effect: corrupts info fields in the graph
       * @return adjacency matrix
       */
      public int[][] createAdjMatrix() {
         info = 0;
         Vertex v = first;
         while (v != null) {
            v.info = info++;
            v = v.next;
         }
         int[][] res = new int[info][info];
         v = first;
         while (v != null) {
            int i = v.info;
            Arc a = v.first;
            while (a != null) {
               int j = a.target.info;
               res[i][j]++;
               a = a.next;
            }
            v = v.next;
         }
         return res;
      }

      /**
       * Create a connected simple (undirected, no loops, no multiple
       * arcs) random graph with n vertices and m edges.
       * @param n number of vertices
       * @param m number of edges
       */
      public void createRandomSimpleGraph(int n, int m) {
         if (n <= 0)
            return;
         if (n > 2500)
            throw new IllegalArgumentException("Too many vertices: " + n);
         if (m < n - 1 || m > n * (n - 1) / 2)
            throw new IllegalArgumentException
                    ("Impossible number of edges: " + m);
         first = null;
         createRandomTree(n);       // n-1 edges created here
         Vertex[] vert = new Vertex[n];
         Vertex v = first;
         int c = 0;
         while (v != null) {
            vert[c++] = v;
            v = v.next;
         }
         int[][] connected = createAdjMatrix();
         int edgeCount = m - n + 1;  // remaining edges
         while (edgeCount > 0) {
            Random newRand = new Random();
            int i = (int) (Math.random() * n);  // random source
            int j = (int) (Math.random() * n);  // random target
            if (i == j)
               continue;  // no loops
            if (connected[i][j] != 0 || connected[j][i] != 0)
               continue;  // no multiple edges
            Vertex vi = vert[i];
            Vertex vj = vert[j];

            // Arrows should have weight so we need to add them here.
            int number = newRand.nextInt(100);

            createArc("a" + vi.toString() + "_" + vj.toString(), vi, vj, number);
            connected[i][j] = 1;
            createArc("a" + vj.toString() + "_" + vi.toString(), vj, vi, number);
            connected[j][i] = 1;
            edgeCount--;  // a new edge happily created
         }
         getVertices();
      }

      /**
       * Create a clone to an existing graph.
       *
       * @param newGraphName is name which will be given to the cloned graph.
       * @throws IllegalStateException, when given graph is empty.
       *
       * @return cloned graph.
       */

      public Graph cloneGraph(String newGraphName) {

         int[][] adj = this.createAdjMatrix();
         Graph clone = new Graph(newGraphName);
         HashMap<String, Vertex> vertices = new HashMap<>();
         HashMap<String, Arc> oldArcs = new HashMap<>();
         Vertex currentRoot = this.first;

         if (currentRoot == null) {
            throw new IllegalStateException("The graph is empty!");
         }
         Arc currentConnection = this.first.first;

         for (int i = adj.length; i > 0 ; i--) {
            Vertex vertex = clone.createVertex(String.valueOf(i));
            vertices.put(String.valueOf(i), vertex);
            oldArcs.put(currentRoot.name, currentConnection);
            if (i > 1) {
               currentRoot = currentRoot.next;
               currentConnection = currentRoot.first;
            }
         }
         Stack<Vertex> arcs = new Stack<>();

         for (int i = adj.length; i > 0; i--) {
            currentRoot = vertices.get(String.valueOf(i));
            Stack<Integer> weights = new Stack<>();
            currentConnection = oldArcs.get(currentRoot.name);

            while (currentConnection != null) {
               Vertex to = vertices.get(currentConnection.target.name);
               arcs.push(to);
               weights.push(currentConnection.weight);
               currentConnection = currentConnection.next;
            }
            while (!arcs.empty()) {
               Vertex to = arcs.pop();
               Integer weight = weights.pop();
               clone.createArc("a" + i + "_" + to.name, currentRoot, to, weight);
            }
         }
         return clone;
      }

      /**
       * Returns hashcodes of vertices with the same id to visually see, whether they are different or not.
       *
       * @param oldV, original Vertex.
       * @param newV, clone of the original vertex.
       *
       * @return String containing original and cloned vertices hashcode.
       */
      public String areVerticesDifferent(Vertex oldV, Vertex newV) {
         return "Original " + oldV.name + " hashcode: " + oldV.hashCode() + ". Cloned vertix hashcode: " + newV.hashCode();
      }

      /**
       * Returns hashcodes of arcs with the same id to visually see, whether they are different or not.
       *
       * @param oldA, original arc.
       * @param newA, clone of the original arc.
       *
       * @return String containing original and cloned arcs hashcode.
       */
      public String areArcsDifferent(Arc oldA, Arc newA) {
         return "Original " + oldA.name + " hashcode: " + oldA.hashCode() + ". Cloned arc hashcode: " + newA.hashCode();
      }

      /**
       * Create reflexive transitive closure matrix of this graph.
       * It is later used to create reflexive transitive closure graph.
       * Side effect: corrupts info fields in the graph
       *
       * @return Matrix that represents reflexive transitive closure of this graph
       */
      public int[][] createReflexiveTransitiveClosureMatrix() {
         info = 0;
         Vertex v = first;
         while (v != null) {
            v.info = info++;
            v = v.next;
         }

         int[][] refTransMatrix = new int[info][info];
         int[][] adjMatrix = createAdjMatrix();

         // copy one matrix to another
         for (int i = 0; i < info; i++)
            for (int j = 0; j < info; j++)
               refTransMatrix[i][j] = adjMatrix[i][j];

         for (int k = 0; k < info; k++) {
            for (int i = 0; i < info; i++) {
               for (int j = 0; j < info; j++) {
                  refTransMatrix[i][j] = (refTransMatrix[i][j] != 0) || ((refTransMatrix[i][k] != 0) && (refTransMatrix[k][j] != 0)) || (i == j) ? 1 : 0;
               }
            }
         }
         return refTransMatrix;
      }

      /**
       * Create reflexive transitive closure of this graph.
       * @return New graph object that is reflexive transitive closure of this graph
       */
      public Graph createReflexiveTransitiveClosureGraphWithoutWeights() {
         int[][] matrix = createReflexiveTransitiveClosureMatrix();
         Graph reflexiveTransitiveClosureGraph = new Graph("Reflexive transitive closure of graph: " + this.name);

         Vertex[] varray = new Vertex[matrix.length];
         for (int i = matrix.length-1; i >= 0; i--) {
            varray[i] = reflexiveTransitiveClosureGraph.createVertex("v" + (i + 1));
         }

         for (int i = matrix.length-1; i >= 0; i--) {
            for (int j = matrix.length-1; j >= 0; j--) {
               if (matrix[i][j] == 1) {
                  reflexiveTransitiveClosureGraph.createArc("a" + varray[i].toString() + "_" + varray[j].toString(), varray[i], varray[j]);
               }
            }
         }
         return reflexiveTransitiveClosureGraph;
      }

      /** Find minimum spanning tree (MST) using Kruskal's algorithm and
       * Galler-Fisher method (or Disjoint Set datastructure).
       * @return A path which is list of arcs
       */
      public List<Arc> kruskalMST() {
         // Sort arcs by increasing weight.
         sortArcs();

         // Create disjoint sets of every vertex referencing to itself and add them to forest.
         for (Vertex v: vertices) {
            forest.add(makeSet(v));
         }

         List<Arc> path = new ArrayList<>();

         // Iterate through arcs till path length is sufficient for all vertices to be connected.
         // If vertices are not connected (i.e belong to different disjoint sets),
         // add edge to path and unite sets.
         int edgesCount = vertices.size() - 1;
         for (Arc a: arcs) {
            if (path.size() == edgesCount) {
               break;
            }
            if (!connected(a.from, a.target)) {
               path.add(a);
               union(a.from, a.target);
            }
         }
         return path;
      }

      /** Sort the arcs by weight in increasing weight order
       * using Merge sort.
       * (Sorting is done in-place and thus doesn't return new list).
       */
      private void sortArcs() {
         Arc[] arcsToSort = new Arc[arcs.size()];
         arcs.toArray(arcsToSort);

         mergeSort(arcsToSort, arcs.size());

         arcs = new ArrayList<>(Arrays.asList(arcsToSort));
      }

      /** Create a disjoint set from vertex, which references to itself.
       * @param v vertex
       * @return new disjoint set of v
       */
      private DisjointSet makeSet(Vertex v) {
         DisjointSet disjointSet = new DisjointSet();
         disjointSet.setParent(v);

         return disjointSet;
      }

      /** Return whether given vertices are connected (i.e belong to the same disjoint set).
       * @param v vertex one
       * @param u vertex two
       * @return true if given vertices are connected
       */
      private boolean connected(Vertex v, Vertex u) {
         return find(v).equals(find(u));
      }

      /** Return disjoint set representative (or parent) containing vertex v
       * using path compression.
       * Source: https://www.baeldung.com/java-spanning-trees-kruskal#find
       * @param v vertex to find
       * @return root of set containing given vertex
       */
      private Vertex find(Vertex v) {
         if (v == null) {
            throw new IllegalArgumentException("Vertex cannot be null");
         }

         // Since vertices and forest elements are parallel,
         // we can get the disjoint set containing given vertex by
         // getting its index from list of vertices.
         int setInd = vertices.indexOf(v);
         Vertex parent = forest.get(setInd).getParent();

         // Return if given vertex is root
         if (parent.equals(v)) {
            return v;
         }

         // Implementing path compression,
         // by attaching root vertex as parent to every subsequent vertex.
         Vertex newParent = find(parent);
         forest.get(setInd).setParent(newParent);

         return newParent;
      }

      /** Unite sets containing vertex v and vertex u.
       * Union is achieved by setting the root of one parent vertex to the other parent vertex.
       * In this case, root of v is always set as the parent to root of u.
       * Source: https://www.baeldung.com/java-spanning-trees-kruskal#union
       * @param v vertex one
       * @param u vertex two
       */
      private void union(Vertex v, Vertex u) {
         Vertex vRoot = find(v);
         Vertex uRoot = find(u);

         // Return if roots are same
         if (vRoot.equals(uRoot)) return;

         int rootInd = vertices.indexOf(uRoot);
         DisjointSet disjointSet = forest.get(rootInd);
         disjointSet.setParent(vRoot);
      }

      /** Merge sort (including method merge())
       * Source: https://www.baeldung.com/java-merge-sort
       */
      private void mergeSort(Arc[] a, int n) {
         if (n < 2) {
            return;
         }
         int mid  = n / 2;
         Arc[] l = new Arc[mid];
         Arc[] r = new Arc[n - mid];

         for (int i = 0; i < mid; i++) {
            l[i] = a[i];
         }
         for (int i = mid; i < n; i++) {
            r[i - mid] = a[i];
         }
         mergeSort(l, mid);
         mergeSort(r, n - mid);

         merge(a, l, r, mid, n - mid);
      }

      public void merge(Arc[] a, Arc[] l, Arc[] r, int left, int right) {
         int i = 0, j = 0, k = 0;
         while (i < left && j < right) {
            if (l[i].weight <= r[j].weight) {
               a[k++] = l[i++];
            }
            else {
               a[k++] = r[j++];
            }
         }
         while (i < left) {
            a[k++] = l[i++];
         }
         while (j < right) {
            a[k++] = r[j++];
         }
      }

      /**
       * Creates minimum spanning tree of the given Graph. All Vertexes ara connected only once and all arrows weight sum
       * is minimal possible to connect vertexes together.
       * @return Graph with all Vertexes connected together once and minimal arrows weights.
       */
      public Graph minimumSpanningTree() {

         Vertex[] createdVertexes = relatedEmptyVertexes(false);  // Array of all final Vertexes
         Vertex[] usedVertexes = relatedEmptyVertexes(true);  // Array of already used Vertexes
         Arc[] allRelatedArrows = new Arc[0];  // All related Arrows of used Vertexes.

         Vertex currentVertex = this.first;
         for (int x = 0; x < usedVertexes.length - 1; x++) {  // Looking for shortest paths

            usedVertexes[x] = currentVertex;  // Add current Vertex as used.

            // Move new relations to arrow array
            Arc[] vertexArrows = currentVertex.first.getRelatedArcs();
            int prevArraySize = allRelatedArrows.length;
            allRelatedArrows = Arrays.copyOf(allRelatedArrows, allRelatedArrows.length + vertexArrows.length);
            System.arraycopy(vertexArrows, 0, allRelatedArrows, prevArraySize, vertexArrows.length);

            Arc neededArrow = defaultArrow(usedVertexes, allRelatedArrows);  // Needed arrow as default
            neededArrow = minimalArrow(usedVertexes, allRelatedArrows, neededArrow);  // Minimal found Needed arrow

            arcAdder(createdVertexes, neededArrow);  // Add to createdVertexes
            currentVertex = neededArrow.target;
         }
         return new Graph(this.name, createdVertexes[0]);
      }

      /**
       * Add arrow relations to the vertexes. So that arrows are two directional - added to both from and to vertexes.
       * @param createdVertexes - array of vertexes to add arrows.
       * @param neededArrow - Arc (arrow) to add to vertexes.
       */
      public void arcAdder(Vertex[] createdVertexes, Arc neededArrow) {
         for (int y = 0; y < createdVertexes.length; y++) {
            if (createdVertexes[y].name.equals(neededArrow.from.name)) {  // Add arrows to Vertex
               Arc prevFirst = createdVertexes[y].first;
               createdVertexes[y].first = neededArrow;
               createdVertexes[y].first.next = prevFirst;
            }

            if (createdVertexes[y].name.equals(neededArrow.target.name)) {  // Add backward arrow to Vertex
               Arc prevFirst = createdVertexes[y].first;
               Arc newArc = new Arc("a" + neededArrow.target.name + "_" + neededArrow.from, neededArrow.from, prevFirst);
               newArc.weight = neededArrow.weight;
               createdVertexes[y].first = newArc;
            }
         }
      }

      /**
       * Creates an array of Vertex chain length with or without elements in it.
       * @param isEmpty - boolean to define: should return empty array or array with empty Vertexes (only name).
       * @return array of chain length with or without elements.
       */
      public Vertex[] relatedEmptyVertexes(boolean isEmpty) {
         int vertexesNumber = this.first.getAll();  // Number of Vertexes in Graph
         Vertex[] createdVertexes = new Vertex[vertexesNumber];  // All empty Vertexes stored here
         if (!isEmpty) {
            Vertex currentVertex = this.first;
            for (int x = 0; x < vertexesNumber; x++) {
               Vertex newVertex = new Vertex(currentVertex.name);
               createdVertexes[x] = newVertex;  // Empty Vertex to array

               if (x != 0 && createdVertexes[x - 1] != null)
                  createdVertexes[x - 1].next = newVertex;
               currentVertex = currentVertex.next;
            }
         }
         return createdVertexes;
      }

      /**
       * Defines the start value for needed Arc.
       * @param usedVertexes - array of already used Vertexes
       * @param allRelatedArrows - array of before defined Arc-s.
       * @return Arc that should be used as default value.
       */
      public Arc defaultArrow(Vertex[] usedVertexes, Arc[] allRelatedArrows) {

         for (int y = 0; y < allRelatedArrows.length; y++) {  // Need to find shortest arrow and not with used Vertex
            if (!vertexContains(allRelatedArrows[y].target, usedVertexes)) {
               Arc currentArrow = allRelatedArrows[y];
               Arc newArc = new Arc(currentArrow.name, currentArrow.target, null);
               newArc.from = currentArrow.from;
               newArc.weight = currentArrow.weight;
               return newArc;
            }
         }
         return new Arc("Null");
      }

      /**
       * Defines Arc with minimal weight to add for Graph.
       * @param usedVertexes - array of already used Vertexes.
       * @param allRelatedArrows - array of before defined Arc-s
       * @param defaultArrow - default Arc value (needed Arc for now)
       * @return Arc that should be added to Graph.
       */
      public Arc minimalArrow(Vertex[] usedVertexes, Arc[] allRelatedArrows, Arc defaultArrow) {

         Arc neededArrow = defaultArrow;
         int minWeight = defaultArrow.weight;

         for (int y = 0; y < allRelatedArrows.length; y++) {  // Need to find shortest and not with used Vertex
            // System.out.println(allRelatedArrows[y] + " " + allRelatedArrows[y].weight) FOR TESTING
            if (allRelatedArrows[y].weight < minWeight && !vertexContains(allRelatedArrows[y].target, usedVertexes)) {
               Arc currentArrow = allRelatedArrows[y];
               minWeight = currentArrow.weight;
               neededArrow = new Arc(currentArrow.name, currentArrow.target, null);
               neededArrow.weight = currentArrow.weight;
               neededArrow.from = currentArrow.from;
            }
         }
         return neededArrow;
      }

      /**
       * Controls if Vertex already used.
       * @param vertex - current Vertex to control.
       * @param usedVertexes - array of already used Vertexes.
       * @return boolean if it is used or not.
       */
      public boolean vertexContains(Vertex vertex, Vertex[] usedVertexes) {
         for (int x = 0; x < usedVertexes.length; x++) {
            if (usedVertexes[x] == vertex)
               return true;
         }
         return false;
      }
   }
}


